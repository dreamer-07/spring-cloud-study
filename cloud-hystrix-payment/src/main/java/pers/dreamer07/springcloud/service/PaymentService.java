package pers.dreamer07.springcloud.service;

import org.springframework.web.bind.annotation.PathVariable;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-26
 **/
public interface PaymentService {

    /**
     * 模拟请求成功的业务逻辑
     * @return
     */
    public String paymentInfoSuc(Long id);

    /**
     * 模拟请求失败的业务逻辑
     * @return
     */
    public String paymentInfoError(Long id);

    public String paymentCircuitBreaker(Long id);
}

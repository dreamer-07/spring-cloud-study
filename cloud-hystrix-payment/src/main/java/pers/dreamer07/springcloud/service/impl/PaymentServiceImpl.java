package pers.dreamer07.springcloud.service.impl;

import cn.hutool.core.util.IdUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import pers.dreamer07.springcloud.service.PaymentService;

import java.util.concurrent.TimeUnit;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-26
 **/
@Service
public class PaymentServiceImpl implements PaymentService {

    @Override
    public String paymentInfoSuc(Long id) {
        return Thread.currentThread().getName() + " - 请求成功，流水号为: " + id;
    }

    /**
     * {@code HystrixCommand} - 服务降级配置
     *      - fallbackMethod: 指定该业务方法的 fallback(兜底方法)
     *      - commandProperties: 配置该业务方法的一些约束
     *        这里是业务的处理时长，设置为 1 秒
     *      - 在该业务方法中如果出现的异常/超时就会调用 fallback 方法
     * @param id
     * @return
     */
    @Override
    @HystrixCommand(fallbackMethod = "paymentInfoErrorHandler", commandProperties = {
            @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value="5000")
    })
    public String paymentInfoError(Long id) {
        // 模拟业务逻辑处理
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Thread.currentThread().getName() + " - 请求失败, 流水号为:" + id;
    }

    /**
     * paymentInfoError 的兜底方法
     * @return
     */
    public String paymentInfoErrorHandler(Long id){
        return Thread.currentThread().getName() + ": 系统繁忙，请稍后重试";
    }


    //=====服务熔断
    @Override
    @HystrixCommand(fallbackMethod = "paymentCircuitBreakerFallback",commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled",value = "true"), // 是否开启断路器
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "10"), // 请求次数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds",value = "10000"), // 时间窗口期
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value = "60"), // 失败率达到多少后跳闸
    })
    public String paymentCircuitBreaker(@PathVariable("id") Long id) {
        if(id < 0) {
            throw new RuntimeException("******id 不能负数");
        }
        String serialNumber = IdUtil.simpleUUID();

        return Thread.currentThread().getName()+"\t"+"调用成功，流水号: " + serialNumber;
    }

    public String paymentCircuitBreakerFallback(Long id) {
        return "id 不能负数，请稍后再试，/(ㄒoㄒ)/~~   id: " +id;
    }

}

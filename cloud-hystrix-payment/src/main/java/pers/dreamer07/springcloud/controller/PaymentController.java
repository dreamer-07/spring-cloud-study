package pers.dreamer07.springcloud.controller;

import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pers.dreamer07.springcloud.service.PaymentService;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-26
 **/
@RestController
public class PaymentController {

    @Autowired
    private PaymentService paymentService;


    @GetMapping("/payment/get/suc/{id}")
    public String getPaymentInfoSuc(@PathVariable Long id){
        return paymentService.paymentInfoSuc(id);
    }

    @GetMapping("/payment/get/error/{id}")
    public String getPaymentInfoError(@PathVariable Long id){
        return paymentService.paymentInfoError(id);
    }

    @GetMapping("/payment/circuit/breaker/{id}")
    public String circuitBreaker(@PathVariable Long id){
        return paymentService.paymentCircuitBreaker(id);
    }

}

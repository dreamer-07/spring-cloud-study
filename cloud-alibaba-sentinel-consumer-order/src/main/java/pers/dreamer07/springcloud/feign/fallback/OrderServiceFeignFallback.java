package pers.dreamer07.springcloud.feign.fallback;

import org.springframework.stereotype.Component;
import pers.dreamer07.springcloud.feign.OrderServiceFeign;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.model.Payment;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-04
 **/
@Component
public class OrderServiceFeignFallback implements OrderServiceFeign {
    @Override
    public CommonResult<Payment> getPaymentInfo(Long id) {
        return new CommonResult<>(4444, "数据出错啦! -- OrderServiceFeignFallback",new Payment(id, null));
    }
}

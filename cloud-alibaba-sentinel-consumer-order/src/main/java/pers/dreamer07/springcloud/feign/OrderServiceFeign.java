package pers.dreamer07.springcloud.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pers.dreamer07.springcloud.feign.fallback.OrderServiceFeignFallback;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.model.Payment;

/**
 * @program: springcloudstudy
 * @description: 指定 OpenFeign 要调用的服务名称和对应的 fallback 兜底方法类
 * @author: EMTKnight
 * @create: 2021-06-04
 **/
@FeignClient(value = "cloud-alibaba-sentinel-provider-payment", fallback = OrderServiceFeignFallback.class)
public interface OrderServiceFeign {

    @GetMapping("/payment/{id}")
    public CommonResult<Payment> getPaymentInfo(@PathVariable("id") Long id);

}

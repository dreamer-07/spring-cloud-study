package pers.dreamer07.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import pers.dreamer07.springcloud.feign.OrderServiceFeign;
import pers.dreamer07.springcloud.feign.fallback.OrderServiceFeignFallback;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.model.Payment;

import javax.annotation.Resource;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-04
 **/
@RestController
public class OrderController {

    @Autowired
    private RestTemplate restTemplate;

    @Resource
    private OrderServiceFeign orderServiceFeign;

    @Value("${services-uri.cloud-alibaba-sentinel-provider-payment}")
    private String providerServiceUrl;

    @GetMapping("/consumer/openFeign/{id}")
    public CommonResult<Payment> testOpenFeign(@PathVariable Long id){
        return orderServiceFeign.getPaymentInfo(id);
    }

    @RequestMapping("/consumer/fallback/{id}")
    @SentinelResource(value = "fallback", fallback = "fallbackHandler", blockHandler = "fallbackBlockHandler")
    public CommonResult<Payment> fallback(@PathVariable Long id)
    {
        CommonResult<Payment> result = restTemplate.getForObject(providerServiceUrl + "/payment/"+id, CommonResult.class,id);

        if (id == 4) {
            throw new IllegalArgumentException ("IllegalArgumentException,非法参数异常....");
        }else if (result.getData() == null) {
            throw new NullPointerException ("NullPointerException,该ID没有对应记录,空指针异常");
        }

        return result;
    }

    public CommonResult<Payment> fallbackHandler(Long id, Throwable t){
        return new CommonResult<>(400, "出错啦! " + t.getMessage(), null);
    }

    public CommonResult<Payment> fallbackBlockHandler(Long id, BlockException e){
        return new CommonResult<>(400, "服务器繁忙! " + e.getMessage(), null);
    }

}

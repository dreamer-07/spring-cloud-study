package pers.dreamer07.springcloud.controller;

import cn.hutool.core.lang.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.model.Payment;
import sun.util.resources.ga.LocaleNames_ga;

import java.util.HashMap;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-04
 **/
@RestController
public class PaymentController {

    @Value("${server.port}")
    private String port;

    // 模拟数据源
    private HashMap<Long, Payment> dataMap = new HashMap(){{
        put(1L, new Payment(1L, UUID.randomUUID().toString()));
        put(2L, new Payment(2L, UUID.randomUUID().toString()));
        put(3L, new Payment(3L, UUID.randomUUID().toString()));
    }};

    @GetMapping("/payment/{id}")
    public CommonResult<Payment> getPaymentInfo(@PathVariable("id") Long id){
        return new CommonResult<>(200, port + "-获取支付信息成功", dataMap.get(id));
    }

}

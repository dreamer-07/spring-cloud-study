package pers.dreamer07.springcloud.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;


/**
 * @program: springcloudstudy
 * @description: 全局日志记录
 * @author: EMTKnight
 * @create: 2021-05-27
 **/
@Component
@Slf4j
public class MyLogGateWayFilter implements GlobalFilter, Ordered {

    /**
     * 过滤器的具体逻辑
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("new request uri: {}", exchange.getRequest().getURI());
        String useranme = exchange.getRequest().getQueryParams().getFirst("username");
        if (useranme == null || "".equals(useranme)){
            log.warn("username is null");
            // 如果数据不合法，设置对应的响应码并返回响应
            exchange.getResponse().setStatusCode(HttpStatus.NOT_ACCEPTABLE);
            return exchange.getResponse().setComplete();
        }
        // 如果数据合法，就进入下一个过滤器
        return chain.filter(exchange);
    }

    /**
     * 返回当前拦截器的优先级，数值越小越高
     * @return
     */
    @Override
    public int getOrder() {
        return 0;
    }
}

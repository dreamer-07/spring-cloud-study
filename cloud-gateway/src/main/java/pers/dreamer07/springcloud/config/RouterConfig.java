package pers.dreamer07.springcloud.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-27
 **/
@Configuration
public class RouterConfig {

    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder){
        RouteLocatorBuilder.Builder routes = builder.routes();

        // 配置多个路由
        routes
            // 配置路由
            .route(
                // 配置该路由的唯一标识
            "FR_Linkage",
                // 配置该路由的 断言(predicate) 和 路由服务接口地址(uri)
                predicateSpec -> predicateSpec.path("/guonei").uri("http://news.baidu.com"))
            .build();

        return routes.build();
    }


}

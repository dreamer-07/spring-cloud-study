package pers.dreamer07.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-01
 **/
@RestController
@Slf4j
public class ConsumerController {

    @Value("${service-url.nacos-payment}")
    private String nacPaymentServiceUrl;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/consumer/get/payment/port")
    public String getPaymentPort(){
        return restTemplate.getForObject(nacPaymentServiceUrl + "/get/payment/port", String.class);
    }

}

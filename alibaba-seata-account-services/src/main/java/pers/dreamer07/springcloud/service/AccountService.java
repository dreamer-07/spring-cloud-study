package pers.dreamer07.springcloud.service;

import pers.dreamer07.springcloud.domian.Account;
import pers.dreamer07.springcloud.model.CommonResult;

public interface AccountService {

    /**
     * 扣除指定账户的余额
     * @param accountId
     * @param money
     * @return
     */
    public CommonResult<Account> deductMoney(Long accountId, Integer money);

}

package pers.dreamer07.springcloud.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pers.dreamer07.springcloud.domian.Account;
import pers.dreamer07.springcloud.mapper.AccountMapper;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.service.AccountService;

import javax.annotation.Resource;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-06
 **/
@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

    @Resource
    private AccountMapper accountMapper;

    @Override
    public CommonResult<Account> deductMoney(Long accountId, Integer money) {
        log.info("alibaba-seata-account-services: deduct money start - id: {}, money: {}", accountId, money);
        Account account = accountMapper.selectById(accountId);
        if (account == null){
            throw new RuntimeException("账户不存在");
        }
        if (account.getResidue() < money){
            throw new RuntimeException("余额不足");
        }
        account.setResidue(account.getResidue() - money);
        account.setUsed(account.getUsed() + money);
        // 模拟超时异常
//        try {
//            Thread.sleep(20000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        accountMapper.updateById(account);
        log.info("alibaba-seata-account-services: deduct money success");
        return new CommonResult<>(200, "扣除账户余额成功", account);
    }
}

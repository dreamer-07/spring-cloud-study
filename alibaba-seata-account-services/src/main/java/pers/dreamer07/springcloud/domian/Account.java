package pers.dreamer07.springcloud.domian;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-06
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_account")
public class Account {

    private Long id;

    private Long userId;

    private Integer total;

    private Integer used;

    private Integer residue;

}

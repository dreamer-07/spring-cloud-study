package pers.dreamer07.springcloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.dreamer07.springcloud.domian.Account;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.service.AccountService;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-06
 **/
@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PutMapping("/account/deduct/{accountId}/{money}")
    public CommonResult<Account> updateAccountMoney(@PathVariable Long accountId, @PathVariable Integer money){
        return accountService.deductMoney(accountId, money);
    }

}

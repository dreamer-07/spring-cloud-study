package pers.dreamer07.springcloud.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import pers.dreamer07.springcloud.domian.Account;

/**
 * @author  EMTKnight
 */
@Mapper
public interface AccountMapper extends BaseMapper<Account> {
}

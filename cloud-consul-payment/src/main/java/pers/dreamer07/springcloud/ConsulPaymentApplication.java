package pers.dreamer07.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-25
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class ConsulPaymentApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsulPaymentApplication.class, args);
    }

}

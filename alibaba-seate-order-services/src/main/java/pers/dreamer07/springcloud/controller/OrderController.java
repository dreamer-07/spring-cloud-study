package pers.dreamer07.springcloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.dreamer07.springcloud.domian.Order;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.service.OrderService;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-07
 **/
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/order/creata")
    public CommonResult<Order> addOrder(Order order){
        return orderService.addOrder(order);
    }

}

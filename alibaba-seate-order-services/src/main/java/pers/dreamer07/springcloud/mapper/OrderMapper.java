package pers.dreamer07.springcloud.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import pers.dreamer07.springcloud.domian.Order;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-07
 **/
@Mapper
public interface OrderMapper extends BaseMapper<Order> {
}

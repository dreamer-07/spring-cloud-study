package pers.dreamer07.springcloud.service;

import pers.dreamer07.springcloud.domian.Order;
import pers.dreamer07.springcloud.model.CommonResult;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-06
 **/
public interface OrderService {

    public CommonResult<Order> addOrder(Order order);

    public CommonResult<Order> updateOrderStatus(Long id);

}

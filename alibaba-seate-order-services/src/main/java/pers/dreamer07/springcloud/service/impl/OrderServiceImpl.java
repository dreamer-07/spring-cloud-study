package pers.dreamer07.springcloud.service.impl;

import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.dreamer07.springcloud.domian.Order;
import pers.dreamer07.springcloud.feign.AccountFeignClient;
import pers.dreamer07.springcloud.feign.StorageFeignClient;
import pers.dreamer07.springcloud.mapper.OrderMapper;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.service.OrderService;

import javax.annotation.Resource;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-06
 **/
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderMapper orderMapper;

    @Autowired
    private AccountFeignClient accountFeignClient;

    @Autowired
    private StorageFeignClient storageFeignClient;

    @Override
    @GlobalTransactional(name = "test-create-order", rollbackFor = Exception.class)
    public CommonResult<Order> addOrder(Order order) {
        log.info("order service - add order start");
        order.setStatus(0);
        orderMapper.insert(order);

        log.info("storage service - update storage stock start");
        storageFeignClient.updateStorageStock(order.getProductId(), order.getCount());
        log.info("storage service - update storage stock success");

        log.info("account service - update account money start");
        accountFeignClient.updateAccountMoney(order.getUserId(), order.getMoney().intValue());
        log.info("account service - update account money success");

        updateOrderStatus(order.getId());
        log.info("order service - add order success");
        return new CommonResult<>(200, "添加订单成功", order);
    }

    @Override
    public CommonResult<Order> updateOrderStatus(Long id) {
        Order order = orderMapper.selectById(id);
        if (order == null){
            return new CommonResult<>(400, "订单信息不存在", null);
        }
        order.setStatus(1);
        orderMapper.updateById(order);
        return new CommonResult<>(200, "订单支付成功", order);
    }
}

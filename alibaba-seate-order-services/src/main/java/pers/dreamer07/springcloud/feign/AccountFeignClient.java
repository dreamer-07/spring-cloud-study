package pers.dreamer07.springcloud.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import pers.dreamer07.springcloud.model.CommonResult;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-06
 **/
@FeignClient(name = "alibaba-seata-account-services")
public interface AccountFeignClient {

    @PutMapping("/account/deduct/{accountId}/{money}")
    CommonResult updateAccountMoney (@PathVariable("accountId") Long accountId, @PathVariable("money") Integer money);

}

package pers.dreamer07.springcloud.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import pers.dreamer07.springcloud.model.CommonResult;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-06
 **/
@FeignClient(name = "alibaba-seata-storage-services")
public interface StorageFeignClient {

    @PutMapping("/storage/stock/{productId}/{count}")
    CommonResult updateStorageStock(@PathVariable("productId") Long productId, @PathVariable("count") Integer count);

}

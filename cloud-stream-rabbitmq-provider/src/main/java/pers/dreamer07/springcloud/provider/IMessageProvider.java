package pers.dreamer07.springcloud.provider;

/**
 * @program: springcloudstudy
 * @description: 定义消息生产者接口
 * @author: EMTKnight
 * @create: 2021-05-31
 **/
public interface IMessageProvider {

    public String send();

}

package pers.dreamer07.springcloud.provider.impl;

import com.netflix.discovery.converters.Auto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import pers.dreamer07.springcloud.provider.IMessageProvider;

import java.util.UUID;

/**
 * @program: springcloudstudy
 * @description: 实现消息生产者接口的规范
 * @author: EMTKnight
 * @create: 2021-05-31
 **/
@EnableBinding(Source.class) // 定义消息的通信管道
@Slf4j
public class MessageProviderImpl implements IMessageProvider {

    // 注入消息发送管道
    @Autowired
    private MessageChannel output;

    @Override
    public String send() {
        String uuid = UUID.randomUUID().toString();
        // 向管道中发送消息
        output.send(MessageBuilder.withPayload(uuid).build());
        log.info("send channel: {}", uuid);
        return uuid;
    }

}

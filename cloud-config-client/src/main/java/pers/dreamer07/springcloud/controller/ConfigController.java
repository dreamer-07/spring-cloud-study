package pers.dreamer07.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-28
 **/
@RestController
@RefreshScope
public class ConfigController {

    @Value("${config.info}")
    public String info;

    @GetMapping("/get/config/info")
    public String getConfigInfo(){
        return info;
    }

}

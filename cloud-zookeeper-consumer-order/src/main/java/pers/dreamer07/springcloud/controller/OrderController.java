package pers.dreamer07.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.model.Payment;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-24
 **/
@RestController
public class OrderController {

    @Autowired
    private RestTemplate restTemplate;

    // 使用对应的服务名即可
    private static final String PAYMENT_URL = "http://cloud-zookeeper-payment";

    @GetMapping("/consumer/payment/zk")
    public String paymentInfo(){
        return restTemplate.getForObject(PAYMENT_URL + "/payment/zk", String.class);
    }

}

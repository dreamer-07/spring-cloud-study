package pers.dreamer07.ribbon;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @program: springcloudstudy
 * @description: 替换默认 Ribbon 负载均衡算法
 * @author: EMTKnight
 * @create: 2021-05-25
 **/
@Configuration
public class RibbonRuleConfig {

    @Bean
    public IRule iRule(){
        return new RandomRule();
    }

}

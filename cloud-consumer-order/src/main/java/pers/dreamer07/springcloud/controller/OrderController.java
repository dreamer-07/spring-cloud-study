package pers.dreamer07.springcloud.controller;

import com.netflix.discovery.converters.Auto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import pers.dreamer07.springcloud.mloadbalancer.MLoadBalancer;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.model.Payment;

import java.util.List;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-24
 **/
@RestController
@Slf4j
public class OrderController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private MLoadBalancer mLoadBalancer;
    @Autowired
    private DiscoveryClient discoveryClient;

//    private final String PAYMENT_URL = "http://localhost:9090/";
    // 配置 Eureka 和生产者集群后使用对应的服务名即可
    private final String PAYMENT_URL = "http://CLOUD-PAYMENT/";

    @GetMapping("/consumer/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable Long id){
        return restTemplate.getForObject(PAYMENT_URL + "payment/get/" + id, CommonResult.class);
    }

    @PostMapping("/consumer/payment/create")
    public CommonResult<Payment> createPayment(Payment payment){
        log.info("cloud consumer payment create: {}", payment);
        return restTemplate.postForObject(PAYMENT_URL + "payment/create", payment, CommonResult.class);
    }

    @GetMapping("/consumer/payment/port")
    public String getPaymentPort(){
        // 通过 discoveryClient 获取指定服务的所有实例
        List<ServiceInstance> instances = discoveryClient.getInstances("cloud-payment");
        if (instances != null && instances.size() <= 0){
            return "当前没有可用的服务";
        }
        // 获取对应的实例
        ServiceInstance service = mLoadBalancer.getService(instances);
        // 向服务发送对应的请求得到数据并返回
        return restTemplate.getForObject(service.getUri() + "/payment/port", String.class);
    }
}

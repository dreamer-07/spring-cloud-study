package pers.dreamer07.springcloud.mloadbalancer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @program: springcloudstudy
 * @description: 使用轮询负载算法
 * @author: EMTKnight
 * @create: 2021-05-26
 **/
@Component
@Slf4j
public class RoundRobinLoadBalancer implements MLoadBalancer{

    // AtomicInteger 操作 Integer 数据的原子类
    private AtomicInteger atomicInteger = new AtomicInteger(0);

    /**
     * 获取对应的 rest 接口访问的次数
     * @return
     */
    public int getIndex(){
        // 定义变量
        int current;
        int next;

        do {
            // 获取当前值
            current = atomicInteger.get();
            // 获取当前值+1，但如果当前 rest 接口访问超过了 10 万次就变为 0
            next = current > 100000 ? 0 : current + 1;
        // 原子判断，判断是否有出现数据异常，如果出现了就重试(自旋锁)
        }while (!atomicInteger.compareAndSet(current, next));
        log.info("当前 rest 接口的仿瓷次数: {}", next);
        return next;
    }

    /**
     * 这里实现轮询负载算法的主要逻辑
     * @param serviceInstanceList 服务列表
     * @return
     */
    @Override
    public ServiceInstance getService(List<ServiceInstance> serviceInstanceList) {
        // 求余
        int index = getIndex() % serviceInstanceList.size();
        return serviceInstanceList.get(index);
    }

}

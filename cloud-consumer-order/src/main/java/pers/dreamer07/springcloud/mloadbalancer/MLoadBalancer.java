package pers.dreamer07.springcloud.mloadbalancer;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * 定义负载均衡器的规范
 */
public interface MLoadBalancer {

    /**
     * 从对应的服务列表中根据指定的规范获取对应的服务
     * @param serviceInstanceList 服务列表
     * @return 获取服务
     */
    public ServiceInstance getService(List<ServiceInstance> serviceInstanceList);

}

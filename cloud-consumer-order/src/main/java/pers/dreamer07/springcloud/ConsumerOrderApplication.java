package pers.dreamer07.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import pers.dreamer07.ribbon.RibbonRuleConfig;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-24
 **/
@SpringBootApplication
@EnableEurekaClient
// 当使用特定的服务时使用指定的负载均衡算法
@RibbonClient(name = "CLOUD-PAYMENT", configuration = RibbonRuleConfig.class)
public class ConsumerOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerOrderApplication.class, args);
    }

}

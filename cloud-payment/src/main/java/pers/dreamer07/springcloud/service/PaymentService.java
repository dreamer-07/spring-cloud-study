package pers.dreamer07.springcloud.service;

import pers.dreamer07.springcloud.model.Payment;

/**
 * 定义操作支付信息的业务逻辑
 * @author Prover07
 */
public interface PaymentService {

    /**
     * 将支付信息保存到数据库中
     * @param payment
     * @return
     */
    public int createPayment(Payment payment);

    /**
     * 根据 id 获取对于的支付信息
     * @param id
     * @return
     */
    public Payment getPaymentById(Long id);

}

package pers.dreamer07.springcloud.controller;

import ch.qos.logback.core.util.TimeUtil;
import com.netflix.discovery.converters.Auto;
import io.micrometer.core.instrument.util.TimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.model.Payment;
import pers.dreamer07.springcloud.service.PaymentService;

import java.util.concurrent.TimeUnit;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-24
 **/
@RestController
@Slf4j
public class PaymentController {

    @Value("${server.port}")
    public String port;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private DiscoveryClient discoveryClient;

    @PostMapping("/payment/create")
    public CommonResult<Payment> createPayment(@RequestBody Payment payment){
        log.info("cloud payment create:{}", payment);
        int isSuc = paymentService.createPayment(payment);
        if (isSuc > 0){
            return new CommonResult<>(200, "支付成功", payment);
        }else {
            return new CommonResult<>(400, "支付失败", null);
        }
    }

    @GetMapping("/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable Long id){
        log.info("cloud payment get by id:{}", id);
        Payment payment = paymentService.getPaymentById(id);
        if (payment != null) {
            return new CommonResult<>(200, "查找成功", payment);
        }else{
            return new CommonResult<>(400, "查找失败, 没有该编号的支付信息 - " + id, null);
        }
    }

    /**
     * 查看 Eureka 中的微服务模块信息
     * @return
     */
    @GetMapping("/discovery/service")
    public Object getEurekaInfo(){
        // discoveryClient.getServices() - 获取所有注册的微服务模块名称
        for (String serviceName : discoveryClient.getServices()) {
            log.info("cloud payment discovery service: {}", serviceName);
        }
        // discoveryClient.getInstances() - 获取指定服务名称的实例
        for (ServiceInstance serviceInstance : discoveryClient.getInstances("cloud-payment")) {
            log.info("cloud payment service instances: ip-{}, port-{}, uri-{}",
                    serviceInstance.getHost(), serviceInstance.getPort(), serviceInstance.getUri());
        }
        return discoveryClient;
    }

    @GetMapping("/payment/port")
    public String getPort(){
        return port;
    }

    @GetMapping("/payment/port/timeout")
    public String getPortTimeout(){
        // 故意暂停程序
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return port;
    }
}

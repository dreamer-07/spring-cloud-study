package pers.dreamer07.springcloud.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import pers.dreamer07.springcloud.model.Payment;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-24
 **/
@Mapper
public interface PaymentMapper extends BaseMapper<Payment> {
}

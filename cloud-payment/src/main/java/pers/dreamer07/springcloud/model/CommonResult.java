package pers.dreamer07.springcloud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.omg.CORBA.TRANSACTION_MODE;

import java.io.Serializable;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-24
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonResult<T> implements Serializable {

    private Integer code;
    private String  message;
    private T   data;

    public CommonResult(Integer code, String message){
        this(code, message, null);
    }

}

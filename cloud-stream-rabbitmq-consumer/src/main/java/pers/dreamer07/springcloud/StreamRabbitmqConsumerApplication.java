package pers.dreamer07.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-31
 **/
@SpringBootApplication
@EnableEurekaClient
public class StreamRabbitmqConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(StreamRabbitmqConsumerApplication.class, args);
    }

}

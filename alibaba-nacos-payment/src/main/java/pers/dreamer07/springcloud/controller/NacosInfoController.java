package pers.dreamer07.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-31
 **/
@RestController
public class NacosInfoController {

    @Value("${server.port}")
    private String port;

    @GetMapping("/get/payment/port")
    public String getPaymentPort(){
        return "Payment port is " + port;
    }

}

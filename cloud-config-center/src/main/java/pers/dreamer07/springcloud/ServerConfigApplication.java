package pers.dreamer07.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @program: springcloudstudy
 * @description: 配置服务器, {@code @EnableConfigServer} 开启配置服务器
 * @author: EMTKnight
 * @create: 2021-05-27
 **/
@SpringBootApplication
@EnableEurekaClient
@EnableConfigServer
public class ServerConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerConfigApplication.class, args);
    }

}

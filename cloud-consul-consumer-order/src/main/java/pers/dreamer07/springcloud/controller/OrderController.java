package pers.dreamer07.springcloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-25
 **/
@RestController
public class OrderController {

    @Autowired
    private RestTemplate restTemplate;

    private static final String CONSUL_URL = "http://cloud-consul-payment";

    @GetMapping("/consumer/payment/consul")
    public String paymentInfoByConsul(){
        return restTemplate.getForObject(CONSUL_URL + "/payment/consul", String.class);
    }

}

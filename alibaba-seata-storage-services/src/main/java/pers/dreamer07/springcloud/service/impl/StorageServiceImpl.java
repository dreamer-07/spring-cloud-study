package pers.dreamer07.springcloud.service.impl;

import org.springframework.stereotype.Service;
import pers.dreamer07.springcloud.domian.Storage;
import pers.dreamer07.springcloud.mapper.StorageMapper;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.service.StorageService;

import javax.annotation.Resource;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-07
 **/
@Service
public class StorageServiceImpl implements StorageService {

    @Resource
    private StorageMapper storageMapper;

    @Override
    public CommonResult<Storage> updateStorageStock(Long productId, Integer count) {
        Storage storage = storageMapper.selectById(productId);
        if (storage == null){
            throw new RuntimeException("商品数据异常");
        }
        Integer residue = storage.getResidue();
        if (residue < count){
            throw new RuntimeException("商品个数不足");
        }
        storage.setUsed(storage.getUsed() + count);
        storage.setResidue(residue - count);
        storageMapper.updateById(storage);
        return new CommonResult<>(200, "修改商品信息成功", storage);
    }

}

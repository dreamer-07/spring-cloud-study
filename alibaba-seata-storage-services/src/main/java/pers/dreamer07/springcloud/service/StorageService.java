package pers.dreamer07.springcloud.service;

import pers.dreamer07.springcloud.domian.Storage;
import pers.dreamer07.springcloud.model.CommonResult;

public interface StorageService {

    public CommonResult<Storage> updateStorageStock(Long productId, Integer count);

}

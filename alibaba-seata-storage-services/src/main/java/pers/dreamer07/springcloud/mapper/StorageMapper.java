package pers.dreamer07.springcloud.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import pers.dreamer07.springcloud.domian.Storage;

@Mapper
public interface StorageMapper extends BaseMapper<Storage> {
}

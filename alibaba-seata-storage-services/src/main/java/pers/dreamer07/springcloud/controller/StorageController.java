package pers.dreamer07.springcloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.service.StorageService;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-07
 **/
@RestController
public class StorageController {

    @Autowired
    private StorageService storageService;

    @PutMapping("/storage/stock/{productId}/{count}")
    CommonResult updateStorageStock(@PathVariable("productId") Long productId, @PathVariable("count") Integer count){
        return storageService.updateStorageStock(productId, count);
    }

}

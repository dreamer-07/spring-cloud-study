package pers.dreamer07.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-03
 **/
@RestController
public class HotKeyController {

    /**
     * {@code @SentinelResource}: 配置有关 Sentinel 的属性
     *      - value: 指定资源名，也是唯一标识
     *      - blockHandler: 针对 sentinel 控制台配置异常
     * @param p1
     * @param p2
     * @return
     */
    @SentinelResource(value = "testHotKey", blockHandler = "blockGetHotKeyInfo")
    @GetMapping("/test/hotkey")
    public String getHotKeyInfo(
            @RequestParam(required = false) String p1,
            @RequestParam(required = false) String p2
    ){
        return "------> HotKeyInfo";
    }

    public String blockGetHotKeyInfo(String p1, String p2, BlockException exception){
        return "------> blockGetHotKeyInfo";
    }

}

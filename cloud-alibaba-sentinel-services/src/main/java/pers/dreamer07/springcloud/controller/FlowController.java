package pers.dreamer07.springcloud.controller;

import cn.hutool.core.thread.ThreadUtil;
import io.micrometer.core.instrument.util.TimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.dreamer07.springcloud.service.FlowService;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-03
 **/
@RestController
@Slf4j
public class FlowController {

    @Autowired
    private FlowService flowService;

    @GetMapping("/test1")
    public String getTest1Info(){
        return "頑張らなくちゃはいけません";
    }

    @GetMapping("/test2")
    public String getTest2Info(){
        log.info("test2 is starting.....");
        return "これからずっと頑張っていく";
    }

    @GetMapping("/test3")
    public String getTest3Info(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "芜湖起飞";
    }

    @GetMapping("/chain1")
    public String testChain1(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return flowService.sentinelChain();
    }

    @GetMapping("/chain2")
    public String testChain2(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return flowService.sentinelChain();
    }

    @GetMapping("/error/ratio")
    public String testErrorRatio(){
        log.info("test ERROR_RATIO(异常比例)");
        int i = 10 / 0;
        return flowService.sentinelChain();
    }
}

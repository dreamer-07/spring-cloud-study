package pers.dreamer07.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.dreamer07.springcloud.handler.RateLimitHandler;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.model.Payment;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-03
 **/
@RestController
public class RateLimitController {

    /**
     * {@code @SentinelResource} 的使用
     *      - value: 资源名
     *      - blockHandler: 针对 Sentinel 控制台配置出错的兜底方法
     * @return
     */
    @SentinelResource(value = "byResources", blockHandler = "getResourcesInfoHandler")
    @GetMapping("/byResources")
    public CommonResult<Payment> getResourcesInfo(){
        return new CommonResult<>(200, "按资源名称作为 Sentinel 配置", new Payment(2021L, "阿巴阿巴"));
    }

    public CommonResult<Payment> getResourcesInfoHandler(BlockException blockException){
        return new CommonResult<>(400, blockException.getMessage() + " - 服务不可用");
    }

    /**
     * {@code @SentinelResource} 的使用: 将接口的地址作为 Sentinel 控制台配置
     * @return
     */
    @SentinelResource(value = "byUrl")
    @GetMapping("/byUrl")
    public CommonResult<Payment> getUrlInfo(){
        return new CommonResult<>(200, "根据 URL 作为 Sentinel 配置", new Payment(2022L, "阿巴巴"));
    }

    /**
     * 测试自定义的全局统一 fallback
     *  blockHandlerClass: 配置全局统一 fallback 类
     *  blockHandler: 配置指定的 fallback 方法
     * @return
     */
    @SentinelResource(value = "globalHandler",
            blockHandlerClass = RateLimitHandler.class, blockHandler = "handler01")
    @GetMapping("/test/globalHandler")
    public CommonResult<Payment> getGlobalHandler(){
        return new CommonResult<>(200, "全局 fallback", new Payment(2023L, "阿巴巴巴"));
    }

}

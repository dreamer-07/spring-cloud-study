package pers.dreamer07.springcloud.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.stereotype.Service;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-03
 **/
@Service
public class FlowService {

    @SentinelResource("sentinelChain")
    public String sentinelChain() {
        return "Sentinel Mode - Chain";
    }

}

package pers.dreamer07.springcloud.handler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.stereotype.Component;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.model.Payment;

/**
 * @program: springcloudstudy
 * @description: 全局统一的 fallback() 方法
 * @author: EMTKnight
 * @create: 2021-06-03
 **/
@Component
public class RateLimitHandler {

    /**
     * 编写自定义的符合业务的处理方法 --> handler01
     * @param exception
     * @return
     */
    public static CommonResult<Payment> handler01(BlockException exception){
        return new CommonResult<>(444, "全局统一的处理方法: handler01()");
    }

    public static CommonResult<Payment> handler02(BlockException exception){
        return new CommonResult<>(444, "全局统一的处理方法: handler02()");
    }

}

package pers.dreamer07.springcloud.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.model.Payment;

/**
 * @program: springcloudstudy
 * @description: 定义服务依赖接口
 * @author: EMTKnight
 * @create: 2021-05-26
 **/
@Component
// 指定使用的微服务名称
@FeignClient(value = "cloud-payment")
public interface OrderService {

    /**
     * 使用 SpringgMVC 的注解指定要调用的服务接口
     * @param id
     * @return
     */
    @GetMapping("/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id);

    /**
     * 调用需要一定时间内才能完成的业务逻辑代码
     * @return
     */
    @GetMapping("/payment/port/timeout")
    public String getPortTimeout();

}

package pers.dreamer07.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-26
 **/
@SpringBootApplication
@EnableFeignClients
public class OpenFeignConsumerOrder {

    public static void main(String[] args) {
        SpringApplication.run(OpenFeignConsumerOrder.class, args);
    }

}

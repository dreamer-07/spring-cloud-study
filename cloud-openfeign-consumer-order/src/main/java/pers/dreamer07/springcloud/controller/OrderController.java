package pers.dreamer07.springcloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pers.dreamer07.springcloud.model.CommonResult;
import pers.dreamer07.springcloud.model.Payment;
import pers.dreamer07.springcloud.service.OrderService;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-26
 **/
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/consumer/payment/get/{id}")
    public CommonResult<Payment> getPaymentInfo(@PathVariable Long id){
        return orderService.getPaymentById(id);
    }

    @GetMapping("/consumer/payment/port/timeout")
    public String getPortTimeout(){
        return orderService.getPortTimeout();
    }
}

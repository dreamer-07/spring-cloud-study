package pers.dreamer07.springcloud.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-26
 **/
@Configuration
public class FeignConfig {

    @Bean
    public Logger.Level feignlogger(){
        return Logger.Level.FULL;
    }

}

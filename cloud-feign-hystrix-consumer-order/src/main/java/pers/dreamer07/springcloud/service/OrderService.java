package pers.dreamer07.springcloud.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pers.dreamer07.springcloud.service.fallback.OrderServiceFallback;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-26
 **/
@FeignClient(value = "cloud-hystrix-payment", fallback = OrderServiceFallback.class)
@Component
public interface OrderService {

    @GetMapping("/payment/get/suc/{id}")
    public String getPaymentInfoSuc(@PathVariable("id") Long id);


    @GetMapping("/payment/get/error/{id}")
    public String getPaymentInfoError(@PathVariable("id") Long id);



}

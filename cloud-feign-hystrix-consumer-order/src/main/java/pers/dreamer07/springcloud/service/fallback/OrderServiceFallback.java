package pers.dreamer07.springcloud.service.fallback;

import org.springframework.stereotype.Component;
import pers.dreamer07.springcloud.service.OrderService;

/**
 * @program: springcloudstudy
 * @description: OrderService 微服务调用接口失败的 fallback 方法类
 * @author: EMTKnight
 * @create: 2021-05-26
 **/
@Component
public class OrderServiceFallback implements OrderService {

    @Override
    public String getPaymentInfoSuc(Long id) {
        return Thread.currentThread().getName() + "OrderServiceFallback --> getPaymentInfoSuc";
    }

    @Override
    public String getPaymentInfoError(Long id) {
        return Thread.currentThread().getName() + "OrderServiceFallback --> getPaymentInfoError";
    }
}

package pers.dreamer07.springcloud.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pers.dreamer07.springcloud.service.OrderService;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-05-26
 **/
@RestController
//@DefaultProperties(defaultFallback = "globalInfoErrorHandler")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/consumer/payment/get/suc/{id}")
    public String getPaymentInfoSuc(@PathVariable Long id){
        return orderService.getPaymentInfoSuc(id);
    }

//    @HystrixCommand(commandProperties = {
//            @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value="1500")
//    })
    @GetMapping("/consumer/payment/get/error/{id}")
    public String getPaymentInfoError(@PathVariable Long id){
        return orderService.getPaymentInfoError(id);
    }

//    public String getPaymentInfoErrorHandler(@PathVariable("id") Long id){
//        return Thread.currentThread().getName() + ": 服务器繁忙，请稍后重试";
//    }
//
//    public String globalInfoErrorHandler(){
//        return Thread.currentThread().getName() + ": global - 服务器繁忙，请稍后重试";
//    }
}

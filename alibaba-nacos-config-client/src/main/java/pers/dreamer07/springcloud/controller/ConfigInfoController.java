package pers.dreamer07.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: springcloudstudy
 * @description:
 * @author: EMTKnight
 * @create: 2021-06-01
 **/
@RestController
@RefreshScope // 开启 Nacos 的动态刷新功能
public class ConfigInfoController {

    @Value("${config.info}")
    private String configInfo;

    @GetMapping("/get/config/info")
    public String getConfigInfo(){
        return configInfo;
    }

}
